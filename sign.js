const nJwt = require('njwt');
const conf = require('./config');
const secret = `nu4nurn3rm3o4c3hc);asdasru3im`;

module.exports = {
  newToken: function(claims, key) {
    let token = nJwt.create(claims, key).setExpiration(new Date().getTime() + (60*60*1000))
    return token
  },
  key: function() {
    let key = secret;
    return key
  },
  compact: function(token) {
    let compact = token.compact();
    return compact
  }
} 
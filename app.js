// Module dependencies.
const express  = require("express"),
http       = require("http"),
path       = require("path"),
cookie     = require('cookie-parser'),
conf       = require('./config');
const app      = express();
const helpers  = require('express-helpers')(app);

// Routes middleware

const auth = require('./routes/auth');
const view = require('./routes/view');
const user = require('./routes/user');

// All environments
app.set("port", process.env.PORT || 3000);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.set('scrt', conf.secret);
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(cookie());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

// App routes

app.get('/', view.dashboard);
app.get("/dashboard", auth.auth, view.dashboard);
app.get("/dashboard/mens", auth.auth, view.men);
app.get("/dashboard/womens", auth.auth, view.women);
app.get("/dashboard/:category/:id", auth.auth, view.products);
app.get("/dashboard/details/:details/:name/:id", auth.auth, view.details);
app.get("/:id", auth.auth, view.specific);
app.get("/currency/:type/:value", auth.auth, user.value);
app.post("/dashboard/register", auth.register);
app.post("/dashboard/login", auth.login);
app.get("/dashboard/logout", auth.requiredAuth, auth.logout);
app.get("/cart/:user", auth.requiredAuth, auth.auth, user.cart);
app.get("/wishlist/:user", auth.requiredAuth, auth.auth, user.wishlist);
app.post("/dashboard/tocart", auth.requiredAuth, auth.auth, user.tocart);
app.post("/dashboard/towishlist", auth.requiredAuth, auth.auth, user.towishlist);
app.post("/dashboard/removecart", auth.requiredAuth, auth.auth, user.removecart);
app.post("/dashboard/removewish", auth.requiredAuth, auth.auth, user.removewish);
app.post("/dashboard/search", auth.auth, user.search);
app.post("/dashboard/tocartfromwish", auth.requiredAuth, auth.auth, user.tocartfromwish);
app.post("/dashboard/token", auth.token);
app.get("/showsearch/:search", auth.auth, user.showsearch);
app.post("/dashboard/facebook", auth.facebook);
app.post("/dashboard/google", auth.google);
app.post("/dashboard/addreview", auth.requiredAuth, auth.auth, user.addreview);
app.get("/readreview/:product", auth.auth, user.readreview);
// Run server
http.createServer(app).listen(app.get("port"), function() {
// Each time we save our console is cleaned.
process.stdout.write('\x1bc');
  console.log("Express server listening on port " + app.get("port"));
});
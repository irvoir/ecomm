const uuidv4 = require('uuid/v4');
const email = require('nodemailer');
const token = uuidv4();

const transport = email.createTransport({
    host: 'smtp.zoho.eu',
    port: 465,
    secure: true,
    auth: {
        user: 'ecommercetesting@zoho.eu',
        pass: 'ASimo423312k3mijnaisod'
    }
});

module.exports = {
    token: function() {
        let key = token;
        return key
    },
    sendActivation: function(email) {
        let options = {
            from: 'ecommercetesting@zoho.eu',
            to: email,
            subject: `Hello! Please confirm this e-mail.`,
            html: 
            `
            <h1 style="color: red"> Activation token for the e-commerce website.</h1>
            <p> 
                Please copy and paste this token 
                <span style="border: 2px solid #b95; color: black; font-weight: bolder; padding: 5px 10px 5px 10px; margin: 5px;">
                    ${token}
                </span>
                on the register form, to continue your registration process.
            </p>
            `
        };
        transport.sendMail(options, (err, info) => {
            if(err) throw err;
        });
    }
}
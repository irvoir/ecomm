This project has some extra features :

1. Search function, enabled for logged users and public
2. Click on each search suggestion, and it will like to the correct item page so you can add to cart or wishlist
3. Protected routes for logged users
4. Login with email, facebook, google
5. Email token activation for email registration
6. Each product can be added to Wishlist, Shopping Cart, add review
7. Read reviews for public space
8. Username, email, password, basic validation
9. Hash password before saving to database
10. Move items from wishlist to cart
11. See total in $ for the whole shopping card
12. Remove items from shopping cart
13. Search result page
14. Filter search suggestion if its more than 10 items
15. Custom pop-up notification
16. Facebook like and share
17. Custom JWT token authentication
18. Redirect if provided JWT token is invalid
19. Send email notification if product is added to users wishlist (only for email login)
20. Few tests
21. Soap currency integration
22. Logout functionality

// Items 3, 8,9 and 22 are all authentication, so only 1 feature instead of 4.
// Items 10 and 11 work together, so only one feature instead of 2.
// Items 1 and 2 are work together, , so only one feature instead of 2.
//So, in total, 17 instead of 22


// 18 instead of 22
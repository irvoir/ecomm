const email = require('nodemailer');

const transport = email.createTransport({
    host: 'smtp.zoho.eu',
    port: 465,
    secure: true,
    auth: {
        user: 'ecommercetesting@zoho.eu',
        pass: 'ASimo423312k3mijnaisod'
    }
});

module.exports = {
    token: function() {
        let key = token;
        return key
    },
    sendEmail: function(email, name, item) {
        let options = {
            from: 'ecommercetesting@zoho.eu',
            to: email,
            subject: `${item} was added to your wishlist!`,
            html: 
            `
            <h1> Hello ${name}!
            <p> 
                We would like to inform you that your item ${item}, was added successfully to your wishlist.
                Enjoy your shopping!
            </p>
            `
        };
        transport.sendMail(options, (err, info) => {
            if(err) throw err;
        });
    }
}
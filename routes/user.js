const soap = require('../soap');
const mdbClient = require('mongodb').MongoClient;
const validator = require('validator');
const conf = require('../config');

// Data that is used throughout the pages
const data = {
	title: 'Project',
	project: 'e-commerce',
	footer: `Copyright © 2017 OSF Global Services`
}
// Predefined categories as helpers
const category = {
	x: 'mens',
	y: 'womens'
}
// Soap integration for different currencies
exports.value = async function(req, res) {
	// Soap currency integration based on URL params that user sends
	const url = 'http://infovalutar.ro/curs.asmx?wsdl';
	// always transform to upperCase because infovalutar accepts only upperCase strings
	let parx = req.params.type.toUpperCase();
	let params = {
		type: parx,
		value: req.params.value
	}
	// Send two requests, one with USD, one with the currency selected by user
	const argx = { Moneda: 'USD' };
	const args = { Moneda: params.type };
	async function results() {
		let final = [await soap.usd(argx), await soap.currency(args)];
		let [{ GetLatestValueResult : usd }, {GetLatestValueResult : currency}] = final;
		// Transform the results
		let calculate = usd / currency;
		let result = calculate * params.value;
		// Send as JSON to client
		res.json(JSON.stringify(result.toFixed(2)));
	};
	results();
}
// Display users cart
exports.cart = function(req, res) {
	mdbClient.connect(conf.users, async (err, db) => {
		const client = db.collection('people');
		// Get the user
		let find = client.findOne({ username: req.params.user });
		if (Object.is(await find, null)) {
			res.status(401).send();
		} else {
			// Get its cart array
			let { cart: cart } = await find;
			let sum = 0;
			// Make a total of the items from cart
			for(let item of cart) {
				sum += parseFloat(item.price);
			}
			let obj = Object.assign({
				specific: null,
				active: null,
				link: null,
				subcategory: null,
				current: null,
				cart: cart,
				total: sum
			}, data);
			// Send to cart ejs view
			res.render('partials/cart', obj);
		}
		setTimeout(() => db.close(), 10000);
	});
}
// Display users wishlist
exports.wishlist = function(req, res) {
	mdbClient.connect(conf.users, async (err, db) => {
		const client = db.collection('people');
		let find = client.findOne({ username: req.params.user });
		if(Object.is(await find, null)) {
			res.status(401).send();
		} else {
			let { wishlist: wishlist } = await find;
			let obj = Object.assign({
				specific: null,
				active: null,
				link: null,
				subcategory: null,
				current: null,
				remove: 'remove()',
				wishlist: wishlist
			}, data);
			res.render('partials/wishlist', obj);
		}
		setTimeout(() => db.close(), 10000);
	})
}
// Specific method, add items to wishlist, also if there is an email, send notification to email
exports.towishlist = function(req, res) {
	if (req.body) {
		let { price: price, id: id, user: user, name: name, image: image } = req.body;
		let finalId = id.replace(/\//g, "");
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			// First update the cart with the items that comes from client
			let update = client.update(
				{ "username": user },
				{ "$addToSet": { "wishlist": { id: finalId, price: price, name: name, image: image} } },
				{ new: true, upsert: true });
			// Then get the email for that user
			update.then(async (result) => {
				let getEmail = client.aggregate([ 
					{ $match : { username: user } },
					{ $group: { _id : name, email: { $push: '$email' } } }
				]).toArray();
				let [ { _id: aggName, email: [ aggEmail ] } ] = await getEmail;
				// Send the email if it exists
				if (aggEmail) {
					// Send notification that an item has been added to his/her wishlist
					notification.sendEmail(aggEmail, user, aggName);
				}
				res.status(200).send();
			}).catch((err) => {
				res.status(401).send();
			})
			setTimeout(() => db.close(), 10000);
		})
	} else {
		res.status(401).send();
	}
}
// Specific method, add items to cart, show total in USD
exports.tocart = function(req, res, next) {
	if (req.body) {
		let { price: price, id: id, user: user, name: name, image: image } = req.body;
		let idFinal = id.replace(/\//g, "");
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			// Update the cart with items or products that come from client
			let find = client.update(
				{ "username": user },
				{ "$addToSet": { "cart": { id: idFinal, price: price, name: name, image: image} } },
				{ new: true, upsert: true });
			find.then((result) => {
				// Send OK status that everything worked out
				res.status(200).send();
				next();
			}).catch((err) => {
				res.status(401).send();
				next();
			})
			setTimeout(() => db.close(), 10000);
		})
	} else {
		res.status(401).send();
	}
}
// Remove items 1 by 1 from cart
exports.removecart = function(req, res) {
	if(req.body) {
		let { id: id, user: user } = req.body;
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			// Remove one item based on ID
			let find = client.update(
				{ username: user },
				{ $pull: { cart: { id: id } } },
				{ multi: false }
			);
			find.then((result) => {
				res.status(200).send();
			}).catch((err) => {
				res.status(401).send();
			})
			setTimeout(() => db.close(), 10000);
		})
	}
}
// Remove an array of items from wishlist, depends how many user selected
exports.removewish = function(req, res) {
	if(req.body) {
		let { id: id, user: user } = req.body;
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			// Loop through ID's and remove them from wishlist
			for (let item of id) {
				let find = client.update(
					{ username: user },
					{ $pull: { wishlist: { id: item } } },
					{ multi: false }
				);
				find.then((result) => {
					res.status(200).send();
				}).catch((err) => {
					res.status(401).send();
				})
			}
			setTimeout(() => db.close(), 10000);
		})
	}
}
// Search suggestion bar
exports.search = function(req, res) {
	let { input: input } = req.body;
	// Validate the search input
	if (!validator.isAlpha(input)) {
		res.status(401).send();
	} else if(validator.isAlpha(input) && 3 < input.length) {
		mdbClient.connect(conf.database, async (err, db) => {
			const client = db.collection('products');
			// Search database with the help of regular expression
			let found = client.find({
				// i flag means that upper or lower case does not matter, case-insensitive
				name: { '$regex':'.*'+`${input}`+'.*', '$options' : 'i' } 
			}).toArray();
			found.then((result) => {
				// Create an array
				let obj = new Array;
				for (let item of result) {
					// Push the items found into array
					obj.push({ name: item.name, id: item.id });
				}
				// Send the array to client
				res.status(200).send(obj);
			}).catch((err) => {
				res.status(401).send();
			})	
			setTimeout(() => db.close(), 10000);
		})
	}
}
// Display new page with the search results
exports.showsearch = function(req, res) {
	if(req.params) {
		// We display a page of results based on the search param
		let { search: input } = req.params;
		mdbClient.connect(conf.database, async (err, db) => {
			const client = db.collection('products');
			let found = client.find({
				// Same logic as a normal suggestion search
				name: { '$regex':'.*'+`${input}`+'.*', '$options' : 'i' } 
			}).toArray();
			found.then((result) => {
				let searchArr = new Array;
				// Create an object with the results
				for (let item of result) {
					searchArr.push({ 
						name: item.name, 
						id: item.id, 
						price: item.price, 
						image_groups: item.image_groups,
						currency: item.currency,
						short_description: item.short_description
					});
				}
				let obj = Object.assign({
					specific: null,
					active: null,
					link: null,
					subcategory: null,
					current: null,
					categories: null,
					products: searchArr
				}, data);
				// Send them to search page
				res.render('partials/search', obj);
			}).catch((err) => {
				res.status(401).send();
			});
			setTimeout(() => db.close(), 10000);
		});
	} else {
		res.status(401).send();
	}
}
// Move items from the wishlist to cart, also remove them from wishlist db
exports.tocartfromwish = function(req, res) {
	if(req.body) {
		// To transfer items from the wish list to cart
		let { id: id, user: user } = req.body;
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			// Multiple step verifications are needed
			let isUser = client.find({ username: user }).toArray();
			// Make sure that user exists
			if(Object.keys(await isUser).length === 0) {
				res.status(401).send();
			} else {
				// Create a set with the items that need to be transfered to cart
				let arr = new Set;
				mdbClient.connect(conf.database, async (err, db) => {
					const products = db.collection('products');
					// First aggregate the items that come from ID's
					for(let item of id) {
						let getProducts = products.aggregate([ 
							{ $match : { id: item } },
							{ $group: {
								price: { $push: '$price' },
								_id : item,
								name: { $push: '$name' },
								image: { $push: '$image_groups' }
							} }
						]).toArray();
						if (Object.keys(await getProducts).length === 0) {
							res.status(401).send();
						} else {
							// Now push them to cart if everything is alright
							let  [{ _id: id, 
									price: [price], 
									name: [name], 
									image: [ [ { images: [ { link: image } ] } ] ]
								}] = await getProducts;
							finalImage = `http://localhost:3000/images/${image}`;
							let find = client.update(
								{ "username": user },
								{ "$addToSet": { "cart": { id: id, price: price, name: name, image: finalImage } } },
								{ new: true, upsert: true }
							);
							find.then((result) => {
								res.status(200).send();
							}).catch((err) => {
								res.status(401).send();
							});
						}
					}
				});
			};
			setTimeout(() => db.close(), 10000);
		})
	};
}
// Add review per user
exports.addreview = function(req, res) {
	if(req.body) {
		mdbClient.connect(conf.database, async (err, db) => {
			const products = db.collection('products');
			let result = products.findOne({ id: req.body.id });
			// find the product ID
			if (Object.keys(await result).length === 0) {
				res.send(401).send();
			} else {
				// Insert the review as a new object
				let insert = products.update(
					{ "id": req.body.id },
					{ "$addToSet": { "review": { 
						title: req.body.title, 
						content: req.body.content, 
						stars: req.body.stars, 
						user: req.body.username 
					} } },
					{ new: true, upsert: true }
				);
				insert.then((result) => {
					res.status(200).send();
				}).catch((err) => {
					res.status(401).send();
				});
			}
			setTimeout(() => db.close(), 10000);
		})
	}
}
// Read reviews per product
exports.readreview = function(req, res) {
	let param = req.params;
	if (param) {
		mdbClient.connect(conf.database, async (err, db) => {
			const client = db.collection('products');
			// Read reviews prop based on master id of that product
			let find = client.aggregate([ 
				{ $match : { id: `${param.product}` } },
				{ $group: { _id : 1, review: { $push: '$review' } } }
			]).toArray();
			// Destructure and push to HTML to render
			let [ { _id: aggid, review: [ aggReview ] } ] = await find;
			if(aggReview) res.render('partials/reviews', { iter: aggReview });
			else res.status(401).send();
			setTimeout(() => db.close(), 10000);
		})
	}
}
const email = require('../email');
const notification = require('../notification');
const mdbClient = require('mongodb').MongoClient;
const sign = require('../sign');
const crypt = require('bcrypt');
const nJwt = require('njwt');
const conf = require('../config');
const validator = require('validator');

// Basic token check, to get the user from its claims and pass it to other middlewares
exports.auth = function(req, res, next) {
	let { token: token } = req.cookies;
	let secret = sign.key();
	// Verify if token is valid and get the user from token
	nJwt.verify(token, secret, (err, verifiedJwt) => {
		if(err) {
			res.locals.user = null
			next();
		} else {
			let { body: { sub : usr } } = verifiedJwt;
			res.locals.user = usr;
			next();
		}
	})
};

// If token comes out as invalid we redirect to dashboard, used only for 'user-only' middlewares
exports.requiredAuth = function(req, res, next) {
	// Two stages of verification, first the length of the cookie, and then if it is valid
	if(Object.keys(req.cookies).length) {
		let { token: token } = req.cookies;
		let secret = sign.key();
		nJwt.verify(token, secret, (err, verifiedJwt) => {
			// If invalid redirect to dashboard
			if(err) res.redirect('/dashboard');
			// Logged in and valid go further in the chain of middlewares
			else next();
		});
	} else {
		// If no cookie is coming to server, redirect
		res.redirect('/dashboard');
	}
};

// Register middleware, check user && send token via email
exports.register = function(req, res) {
	let { username: usr, email: ema, password: pass, confirm: conf } = req.body;
	// Basic validation, returns 401 if the input on client is invalid
	if (!validator.isAlpha(usr)) {
		res.status(401).send({ error: `The username: ${usr} is not valid. Only alpha characters are allowed.` });
	} else if(!validator.isEmail(ema)) {
		res.status(401).send({ error: 'Your email is incorect.' });
	} else if(!validator.isAlphanumeric(pass)) {
		res.status(401).send({ error: 'Your password is not valid.' });
	} else if(!validator.isAlphanumeric(conf)) {
		res.status(401).send({ error: `Your confirmation password is not valid` })
	} else if (!Object.is(pass, conf)) {
		res.status(401).send({ error: `The passwords doesn't match.` })
	} else {
		// Now every input is valid to be stored in database
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			// Quick validation to be sure there are no duplicates
			let valid = client.find({ $or: [ { username: usr }, { email: ema } ]}).toArray();
			if (Object.keys(await valid).length === 0) {
				// Send an email with activation token (just a basic uuid)
				email.sendActivation(ema);
				res.status(200).send();
			} else {
				// If user or email already exists send error 401
				res.status(401).send();
			}
			setTimeout(() => db.close(), 10000);
		})
	}
};

// Create account if token is valid
exports.token = function(req, res) {
	// Token validation, ensure that both tokens are strictly equal, one comes user input, one is the true generated token
	if(req.body.token === email.token()) {
		let { username: usr, email: ema, password: pass } = req. body;
		mdbClient.connect(conf.users, async function(err, db) {
			const client = db.collection('people');
			// If token is valid, then we can register the user
			crypt.hash(pass, 10).then((hash) => {
				client.ensureIndex( { username: 1 }, { unique: true });
				client.ensureIndex( { email: 1 }, { unique: true} );
				let key = sign.key();
				let insert = client.insert({
					username: usr,
					email: ema,
					// Never store passwords in plain text
					password: hash,
					date: new Date(),
					// Initiate cart and wishlist as empty arrays
					cart: [],
					wishlist: [],
					key: key
				});
				insert.then((result) => {
					res.status(200).send();
				}).catch((err) => {
					res.status(401).send();
				})
			})
			setTimeout(() => db.close(), 10000);
		});
	} else {
		// If token sent from user is invalid
		res.status(401).send();
	}
};

// Login user with email, gives back a token
exports.login = function(req, res, next) {
	let { username: usr, password: pass } = req.body;
	mdbClient.connect(conf.users, async (err, db) => {
		const client = db.collection('people');
		// First step to login, is to be sure that username exists
		let final = client.findOne({ username: usr });
		if (await final) {
			let { password: hash, key: key } = await final;
			// Now safely compare the input password 
			crypt.compare(pass, hash).then((result) => {
				// If password matches, create a token and send it as a cookie
				if(result) {
					let claims = {
						iss: "http://localhost:3000",
						sub: usr,   
						scope: "user"
					}
					let token = sign.newToken(claims, key);
					let compact = sign.compact(token);
					let options = { httpOnly: true };
					res.cookie('token', compact, options);
					res.status(200).send();
				} else {
					res.status(401).send({ err: `Password invalid. ` });
				}
			}).catch((err) => {
				res.status(401).send({ err: `Password invalid.`});
			});
		} else {
			res.status(401).send({ err: `Password invalid. ` });
		}
		setTimeout(() => db.close(), 10000);
	})
};

// To logout, just send an expired cookie
exports.logout = function(req, res) {
	res.cookie('token', null, { expires: new Date(Date.now() - 900000), httpOnly: true });
	res.status(200).send();
};

// Store or read facebook unique ID, send back a token
exports.facebook = function(req, res) {
	if(1 < Object.keys(req.body).length && req.body.constructor === Object) {
		mdbClient.connect(conf.users, async (err, db) => {
			const client = db.collection('people');
			let key = sign.key();
			let claims = {
				iss: "http://localhost:3000",
				sub: req.body.name,   
				scope: "user"
			}
			// Create a new token to be sent through cookies
			let token = sign.newToken(claims, key);
			// Send a compact version, not the full token
			let compact = sign.compact(token);
			// Can't edit it throught javascript
			let options = { httpOnly: true };
			// Find those ID's, to see if they exist
			let exists = client.find({ username: req.body.name, id: req.body.id }).toArray();
			exists.then(async (result) => {
				// If the array returned is empty, user does not exist
				if (Object.keys(await exists).length === 0) {
					let insert = client.insert({
						username: req.body.name,
						id: req.body.id,
						email: req.body.id,
						date: new Date(),
						cart: [],
						wishlist: [],
						key: key
					});
					res.cookie('token', compact, options);
					res.status(200).send();
				} else {
					// Array is not empty, so that user exists
					res.cookie('token', compact, options);
					res.status(200).send();
				}
			// Database errors, will send 401 (can't connect to db)
			}).catch((err) => {
				res.status(401).send();
			});
			setTimeout(() => db.close(), 10000);
		})
	} else if(Object.keys(req.body).length === 0 && req.body.constructor === Object) {
		// if post request has an empty body
		res.status(401).send();
	}
};

// Same method as Facebook, save ID and Name, which are unique
exports.google = function(req, res) {	
	exports.facebook(req, res);
};
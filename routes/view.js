const mdbClient = require('mongodb').MongoClient;
const conf = require('../config');

// Data that is used throughout the pages
const data = {
	title: 'Project',
	project: 'e-commerce',
	footer: `Copyright © 2017 OSF Global Services`
}
// Predefined categories as helpers
const category = {
	x: 'mens',
	y: 'womens'
}

exports.dashboard = function(req, res) {
	res.redirect('/dashboard/mens')
};

exports.men = function(req, res) {
	// Connect to mongo driver
	mdbClient.connect(conf.database, async function(err, db) {
		const client = db.collection('categories');
		if (err) throw err;
		// Perform the search through database
		let results = client.findOne({ id: "mens" }), i = -1;
		let obj = {
			user: res.locals.user || null,
			// findOne returns a cursor asynchronously, transform cursor to an array
			ctx: [await results],
			active: `Men's`,
			link: category.x,
			subcategory: null,
			current: null,
			i: i++
		};
		// Combine the global data object with the specific data for men category
		let final = Object.assign(obj, data);
		// Send to client for views
		res.render('partials/men', final);
		// Close the connection after 10seconds
		setTimeout(() => db.close(), 10000);
	});
}

exports.women = function(req, res) {
	mdbClient.connect(conf.database, async function(err, db) {
		const client = db.collection('categories');
		if (err) throw err;
		let results = client.findOne({ id: "womens" }), i = -1;
		let obj = {
			ctx: [await results],
			active: `Women's`,
			link: category.y,
			subcategory: null,
			current: null,
			i: i++
		};
		let final = Object.assign(obj, data);
		res.render('partials/women', final);
		setTimeout(() => db.close(), 10000);
	});
}

exports.products = function(req, res) {
	mdbClient.connect(conf.database, async function(err, db) {
		if (err) throw err;
		let results = new Array;
		const client = db.collection('categories');
		// Get the whole database and interpret it
		client.find().toArray(async function(err, items) {
			if (err) throw err;
			let obj = {
				active: null,
				link: null,
				subcategory: null,
				current: null,
			};
			// Construct a category for men
			if (req.params.category === category.x) {
				results = await items[0].categories[req.params.id];
				obj.active = `Men's`;
				obj.link = `mens`;
				obj.subcategory = results.name;
				// Construct a category for women
			} else if (req.params.category === category.y) {
				results = await items[1].categories[req.params.id];
				obj.active = `Women's`;
				obj.link = `womens`;
				obj.subcategory = results.name;
			}
			// Send the correct category based on URL params that comes from user selection on client
			let final = Object.assign(results, data, obj);
			res.render('partials/products', final);
			setTimeout(() => db.close(), 10000);
		});
	});
}

exports.details = function(req, res) {
	mdbClient.connect(conf.database, async function(err, db) {
		if(err) throw err;
		const client = db.collection('products');
		let param = {
			x: req.params.details,
			y: req.params.name,
			z: req.params.id
		};
		// User selects primary category ID, now it's easier to give back a response
		let results = client.find({ 
			"primary_category_id": param.x
		}).toArray();
		let end = Object.assign({
			products: await results,
			active: param.z,
			link: undefined,
			subcategory: null,
			current: param.y
		}, data);
		res.render('partials/details', end);
		setTimeout(() => db.close(), 10000);
	})
}

exports.specific = function(req, res) {
	mdbClient.connect(conf.database, async function(err, db) {
		if(err) throw err;
		const client = db.collection('products');
		let results = client.findOne({ id: req.params.id });
		let obj = Object.assign({
			specific: [await results],
			active: null,
			link: undefined,
			subcategory: null,
			current: null
		}, data);
		res.render('partials/specific', obj);
		setTimeout(() => db.close(), 10000);
	})
}
const mdbClient = require('mongodb').MongoClient;
const port = 3000;
const superagent = require('superagent');
const chai = require('chai');
const expect = chai.expect;
const should = chai.should;

describe('#find()', function() {
    it('respond with matching records', function() {
        mdbClient.connect('mongodb://irvoir:galagiemare0@ds113455.mlab.com:13455/shop'), async (err, db) => {
            const client = db.collection('people');
            client.find({ username: 'irvoir' }).should.eventually.have.length(6);
        }
    });
});

describe('homepage', function(){
    it('should respond to GET', (done) => {
      superagent
        .get('http://localhost:' + port)
        .end( (err, res) => {
          expect(res.status).to.equal(200);
          done();
      })
    })
});

describe('facebook', function(){
    it('should respond to POST with 401', (done) => {
      superagent
        .post('http://localhost:' + port + '/dashboard/facebook')
        .end( (err, res) => {
          expect(res.status).to.equal(401);
          done();
      })
    })
});

describe('facebook create account', function(){
    it('should respond to POST with 200 if it has a body id and name', (done) => {
      superagent
        .post('http://localhost:' + port + '/dashboard/facebook')
        .send({ name: 'Manny', id: 'cat' })
        .end( (err, res) => {
          expect(res.status).to.equal(200);
          done();
      })
    })
});

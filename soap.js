const soap = require('soap');
const url = 'http://infovalutar.ro/curs.asmx?wsdl';
// In short the method is: getValue of dollars(our db is always in dollars);
// then divide the requested currency with the value of dollars

exports.usd = function(args) {
    return new Promise((resolve, reject) => {
        soap.createClientAsync(url).then((client) => {
            resolve(client.GetLatestValueAsync(args))
        })
    })
};

exports.currency = function(argx) {
    return new Promise((resolve, reject) => {
        soap.createClientAsync(url).then((client) => {
            resolve(client.GetLatestValueAsync(argx));
        });
    })
};